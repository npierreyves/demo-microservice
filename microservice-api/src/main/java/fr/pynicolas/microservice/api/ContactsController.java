package fr.pynicolas.microservice.api;

import fr.pynicolas.microservice.dao.Contact;
import fr.pynicolas.microservice.dao.ContactRepository;
import fr.pynicolas.microservice.dao.Type;
import fr.pynicolas.microservice.model.ContactsCheckRequest;
import fr.pynicolas.microservice.model.ContactsCheckResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(path = "/contacts")
public class ContactsController {

    private final ContactRepository contactRepository;

    @Autowired
    private ContactsController(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @PostMapping(path = "/check")
    public ContactsCheckResponse check(@RequestBody ContactsCheckRequest body){

        ContactsCheckResponse contactsCheckResponse = new ContactsCheckResponse(body.getMsgId(), new HashMap<String, Type>());
        Map<String, Type> resultContacts = contactsCheckResponse.getContacts();

        for(String contactIdentifier : body.getContacts()) {
            Contact contact = contactRepository.findById(contactIdentifier).orElse(new Contact(contactIdentifier, Type.UNKNOWN));
            resultContacts.put(contact.getId(), contact.getType());
        }

        return contactsCheckResponse;
    }
}