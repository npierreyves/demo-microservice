package fr.pynicolas.microservice.dao;

public enum Type {
    UNKNOWN, ENROLLED, SPECIAL
}
