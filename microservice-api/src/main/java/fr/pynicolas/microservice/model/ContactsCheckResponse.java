package fr.pynicolas.microservice.model;

import fr.pynicolas.microservice.dao.Type;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@Getter
public class ContactsCheckResponse extends AbstractModel {

    private Map<String, Type> contacts;

    @Builder
    public ContactsCheckResponse(String msgId, Map<String, Type> contacts) {
        super(msgId);
        this.contacts = contacts;
    }
}
