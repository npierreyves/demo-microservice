package fr.pynicolas.microservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AbstractModel {

    private String msgId;
}
