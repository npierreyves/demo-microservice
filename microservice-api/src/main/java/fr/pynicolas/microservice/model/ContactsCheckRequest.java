package fr.pynicolas.microservice.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
public class ContactsCheckRequest extends AbstractModel {

    private List<String> contacts;

    @Builder
    public ContactsCheckRequest(String msgId, List<String> contacts) {
        super(msgId);
        this.contacts = contacts;
    }
}
