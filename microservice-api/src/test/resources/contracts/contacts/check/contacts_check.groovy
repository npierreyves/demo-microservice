package contacts;

import org.springframework.cloud.contract.spec.Contract

Contract.make {
  description('''
  Nominal contacts check
  
  given:
     sends a list of contacts
  when:
     the request is well formatted
  then:
     answer with an augmented list
  
  ''')
  request {
    method('POST')
    url("/contacts/check")
    headers {
      contentType(applicationJson())
    }
    body(file("request.json"))

  }
  response {
    status OK()
    headers {
      contentType(applicationJson())
    }
    body(file("response.json"))
  }
}