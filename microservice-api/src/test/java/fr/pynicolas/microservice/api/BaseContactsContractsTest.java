package fr.pynicolas.microservice.api;

import fr.pynicolas.microservice.MicroserviceApplication;
import fr.pynicolas.microservice.dao.Contact;
import fr.pynicolas.microservice.dao.ContactRepository;
import fr.pynicolas.microservice.dao.Type;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MicroserviceApplication.class)
public abstract class BaseContactsContractsTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    @MockBean
    private ContactRepository contactRepository;

    @Before
    public void setup() {
        Contact savedContact = new Contact();
        savedContact.setId("johndoe@gmail.com");
        savedContact.setType(Type.ENROLLED);
        when(contactRepository.save(any(Contact.class))).thenReturn(savedContact);

        RestAssuredMockMvc.webAppContextSetup(webApplicationContext);

        when(contactRepository.findById(eq("johndoe@gmail.com"))).thenReturn(Optional.of(savedContact));
    }

}