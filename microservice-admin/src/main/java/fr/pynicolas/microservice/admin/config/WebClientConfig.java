package fr.pynicolas.microservice.admin.config;

import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import de.codecentric.boot.admin.server.web.client.InstanceExchangeFilterFunction;
import de.codecentric.boot.admin.server.web.client.InstanceWebClient;
import io.netty.channel.ChannelOption;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.Collections;
import java.util.List;

@Configuration
@ConditionalOnProperty(prefix = "webclient", name = "ssl")
public class WebClientConfig
{
    @Value("${webclient.ssl.trust-store}")
    String trustStorePath;
    @Value("${webclient.ssl.trust-store-password}")
    String trustStorePass;
    @Value("${webclient.ssl.trust-store-type}")
    String trustStoreType;
    @Value("${webclient.ssl.key-store}")
    String keyStorePath;
    @Value("${webclient.ssl.key-store-password}")
    String keyStorePass;
    @Value("${webclient.ssl.key-store-type}")
    String keyStoreType;

    @Bean
    public InstanceWebClient instanceWebClient(HttpHeadersProvider httpHeadersProvider,
                                               ObjectProvider<List<InstanceExchangeFilterFunction>> filtersProvider) {
        List<InstanceExchangeFilterFunction> additionalFilters = filtersProvider.getIfAvailable(Collections::emptyList);
        return InstanceWebClient.builder()
                //.connectTimeout(this.adminServerProperties.getMonitor().getConnectTimeout())
                //.readTimeout(this.adminServerProperties.getMonitor().getReadTimeout())
                //.defaultRetries(this.adminServerProperties.getMonitor().getDefaultRetries())
                //.retries(this.adminServerProperties.getMonitor().getRetries())
                .httpHeadersProvider(httpHeadersProvider)
                .filters(filters -> filters.addAll(additionalFilters))
                .webClientCustomizer(new WebClientCustomizer() {
                    @Override
                    public void customize(WebClient.Builder webClientBuilder) {
                        webClientBuilder.clientConnector(create2WayTLSHttpClient());
                    }
                })
                .build();
    }


    private ClientHttpConnector create2WayTLSHttpClient() {

        HttpClient httpClient = HttpClient.create().secure(sslContextSpec -> sslContextSpec.sslContext(get2WaySSLContext()));

        return new ReactorClientHttpConnector(httpClient);

    }

    private SslContext get2WaySSLContext() {

        try {

            //keystore
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(new FileInputStream(ResourceUtils.getFile(keyStorePath)), keyStorePass.toCharArray());
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, keyStorePass.toCharArray());

            //trustore
            KeyStore trustStore = KeyStore.getInstance(keyStoreType);
            trustStore.load(new FileInputStream(ResourceUtils.getFile(trustStorePath)), trustStorePass.toCharArray());
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            //ssl context
            return SslContextBuilder.forClient()
                    .protocols("TLSv1.3")
                    .keyManager(keyManagerFactory)
                    .trustManager(trustManagerFactory)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
