# Demo Spring Boot stack microservice

1. [Total confience](#total-confience)
	1. [Starting Microservice](#starting-microservice)
	2. [Starting Admin](#starting-admin)
2. [Microservice-secure mode](#microservice-secure-mode)
	1. [Starting Microservice](#starting-microservice-1)
	2. [Starting Admin](#starting-admin-1)


## Total confience

This part used "confience" profile for configured all elements without TLS.


```
       8080 				       8181
+-----------------+                      +--------------+
| ADMIN INTERFACE |----------------------| MICROSERVICE |
+-----------------+                   	 +--------------+
```

Set profile with :
```bash
export SPRING_PROFILES_ACTIVE=confience
```

### Starting microservice

Start microservice by runnin the main methode of the [App Class](microservice-api/src/main/java/fr/pynicolas/microservice/MicroserviceApplication.java) in the microservice-api project or by running the following command from the terminal in the root directory :
```bash
gradlew :microservice-api:bootrun
```

Currently, the microserver is running on the default port of 8181 __WITHOUT__ encryption. You can call the actuator/health endpoint with the following curl command in the terminal:

```bash
curl -iv -X GET http://localhost:8181/actuator/health
```

It should give you the following response:

```bash
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8181 (#0)
> GET /actuator/health HTTP/1.1
> Host: localhost:8181
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 
HTTP/1.1 200 
< Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
< Transfer-Encoding: chunked
Transfer-Encoding: chunked
< Date: Tue, 15 Oct 2019 00:14:35 GMT
Date: Tue, 15 Oct 2019 00:14:35 GMT

< 
* Connection #0 to host localhost left intact
{"status":"UP"}
```


### Starting admin

Start admin  by runnin the main methode of the [App Class](microservice-admin/src/main/java/fr/pynicolas/microservice/admin/AdminApplication.java) in the microservice-admin project or by running the following command from the terminal in the root directory :
```bash
gradlew :microservice-admin:bootRun
```

Currently, the admin site is running on the default port of 8080 __WITHOUT__ encryption. You can call the actuator/health endpoint with the following curl command in the terminal:

```bash
curl -iv -X GET http://localhost:8080/actuator/health
```

It should give you the following response:

```bash
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /actuator/health HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
HTTP/1.1 200 OK
< Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
< Content-Length: 15
Content-Length: 15

< 
* Connection #0 to host localhost left intact
{"status":"UP"}
```

## Microservice secure mode

This part used "ms_secure" profile for configured microservice __WITH__ encryption and admin interface __without__ encryption.

```
       8080					       _8181_
+-----------------+           TLS                +--------------+
| ADMIN INTERFACE |------------------------------| MICROSERVICE |
+----------------+ keystore            keystore  +--------------+
                   truststore          trustore
```

Set profile with :
```bash
export SPRING_PROFILES_ACTIVE=ms_secure
```

### Starting microservice

Start microservice by runnin the main methode of the [App Class](/microservice-api/src/main/java/fr/pynicolas/microservice/MicroserviceApplication.java) in the microservice-api project or by running the following command from the terminal in the root directory :
```bash
gradlew :microservice-api:bootrun
```

Currently, the microserver is running on the default port of 8181 without encryption. You can call the actuator/health endpoint with the following curl command in the terminal:

```bash
curl -iv --cacert ./ca.crt --key ./localhost.key --cert ./localhost.crt https://localhost:8181/actuator/health
```

It should give you the following response:

```bash
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8181 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: ./ca.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (OUT), TLS change cipher, Client hello (1):
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
* TLSv1.3 (IN), TLS handshake, Unknown (8):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.3 (OUT), TLS Unknown, Certificate Status (22):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
* ALPN, server did not agree to a protocol
* Server certificate:
*  subject: CN=localhost
*  start date: Oct  1 19:30:48 2019 GMT
*  expire date: Sep 15 19:30:48 2022 GMT
*  subjectAltName: host "localhost" matched cert's "localhost"
*  issuer: CN=pynicolas
*  SSL certificate verify ok.
* TLSv1.3 (OUT), TLS Unknown, Unknown (23):
> GET /actuator/health HTTP/1.1
> Host: localhost:8181
> User-Agent: curl/7.58.0
> Accept: */*
> 
* TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.3 (IN), TLS Unknown, Unknown (23):
< HTTP/1.1 200 
< Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 15 Oct 2019 00:04:37 GMT
< 
* TLSv1.3 (IN), TLS Unknown, Unknown (23):
* Connection #0 to host localhost left intact
{"status":"UP"}p
```

### Starting admin

Start admin  by runnin the main methode of the [App Class](microservice-admin/src/main/java/fr/pynicolas/microservice/admin/AdminApplication.java) in the microservice-admin project or by running the following command from the terminal in the root directory :
```bash
gradlew :microservice-admin:bootRun
```

Currently, the admin site is running on the default port of 8080 __WITHOUT__ encryption. You can call the actuator/health endpoint with the following curl command in the terminal:

```bash
curl -iv -X GET http://localhost:8080/actuator/health
```

It should give you the following response:

```bash
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /actuator/health HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
HTTP/1.1 200 OK
< Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
Content-Type: application/vnd.spring-boot.actuator.v2+json;charset=UTF-8
< Content-Length: 15
Content-Length: 15

< 
* Connection #0 to host localhost left intact
{"status":"UP"}
```


